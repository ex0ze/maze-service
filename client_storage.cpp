/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "client_storage.hpp"

#include <boost/asio/post.hpp>

#include "maze/image/image.hpp"
#include "maze/serialization/serialization.h"

namespace
{

    std::string_view generated_algo_to_string(contracts::GenerationAlgorithm algorithm) {
        using algo = contracts::GenerationAlgorithm;
        switch (algorithm)
        {
        case algo::Aldous_Broder:
            return "Aldous_Broder";
        case algo::Kruskal:
            return "Kruskal";
        case algo::Prim:
            return "Prim";
        default:
            throw std::runtime_error("No algo");
        }
    }

    std::string_view generated_algo_to_string(contracts::SolutionAlgorithm algorithm) {
        using algo = contracts::SolutionAlgorithm;
        switch (algorithm) {
            case algo::Dijkstra: return "Dijkstra";
            case algo::DeadEnd: return "Dead End";
            case algo::WallFollower: return "Wall Follower";
            default: return "Unknown";
        }
    }

} // anonymous namespace

client_storage::client_storage(boost::asio::thread_pool& thread_pool)
: thread_pool_(thread_pool) {}

void client_storage::insert_and_save_generated(std::size_t id, contracts::MazeGeneratorResponse generated, callback_type cb)
{
    auto task = [id, generated = std::move(generated), cb = std::move(cb), this] {
        {
            std::lock_guard<std::mutex> _(mutex_);
            storage_[id].generated_image_status = stored_type::image_saving;
        }
        stored_type result;
        result.generated_image_status = stored_type::image_saved;
        const auto &header = generated.header();
        result.generated_area = maze::unsolved_from_bytes(generated.maze(),
                                                          header.height(), header.width());
        auto path = std::filesystem::current_path() / "images";
        result.generated_image_path = maze::save_as_image(result.generated_area,
                                                          path.string(), generated_algo_to_string(generated.algorithm()), false);
        result.generated = std::move(generated);
        {
            std::lock_guard<std::mutex> _(mutex_);
            storage_[id] = std::move(result);
        }
        cb();
    };
    boost::asio::post(thread_pool_, std::move(task));
}

void client_storage::insert_and_save_solved(std::size_t id, contracts::MazeSolverResponse solved, callback_type cb)
{
    if (storage_[id].solved_image_status != stored_type::no_image)
        throw std::runtime_error(__func__);
    auto task = [id, solved = std::move(solved), cb = std::move(cb), this] {
        stored_type result;
        {
            std::lock_guard<std::mutex> _(mutex_);
            result = std::move(storage_[id]);
            storage_[id].solved_image_status = stored_type::image_saving;
        }
        result.solved_image_status = stored_type::image_saved;
        const auto& header = solved.header();
        maze::solved_from_bytes(solved.solution(),
            header.height(),
            header.width(),
            result.generated_area);

        auto p = std::filesystem::current_path() / "images";
        result.solved_image_path = maze::save_as_image(result.generated_area,
            p.string(),
            generated_algo_to_string(solved.algorithm()), true);
        result.generated_area.clear();
        result.generated.Clear();
        result.solved_area.clear();
        result.solved.Clear();
        {
            std::lock_guard<std::mutex> _(mutex_);
            storage_[id] = std::move(result);
        }
        cb();
    };
    boost::asio::post(thread_pool_, std::move(task));
}

bool client_storage::is_generated_saved(std::size_t id) const
{
    std::lock_guard<std::mutex> _(mutex_);
    if (auto it = storage_.find(id); it != storage_.end())
        return (it->second.generated_image_status == stored_type::image_saved);
    else
        return false;
}

bool client_storage::is_solved_saved(std::size_t id) const
{
    std::lock_guard<std::mutex> _(mutex_);
    if (auto it = storage_.find(id); it != storage_.end())
        return (it->second.solved_image_status == stored_type::image_saved);
    else
        return false;
}

bool client_storage::is_solved_saving(std::size_t id) const
{
    std::lock_guard<std::mutex> _(mutex_);
    if (auto it = storage_.find(id); it != storage_.end())
        return (it->second.solved_image_status == stored_type::image_saving);
    else
        return false;
}

std::string client_storage::generated_image_path(std::size_t id) const
{
    std::lock_guard<std::mutex> _(mutex_);
    return storage_.at(id).generated_image_path;
}

std::string client_storage::solved_image_path(std::size_t id) const
{
    std::lock_guard<std::mutex> _(mutex_);
    return storage_.at(id).solved_image_path;
}
