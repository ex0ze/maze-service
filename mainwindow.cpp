#include "mainwindow.hpp"
#include "./ui_mainwindow.h"

#include <QAbstractEventDispatcher>
#include <QMenu>
#include <QMessageBox>
#include <QThread>

#include "proto/maze.pb.h"
#include "maze/serialization/serialization.h"

#include "pizza/contracts/AnyMessage.pb.h"

using namespace std::chrono_literals;

namespace {

contracts::GenerationAlgorithm generation_algo_for_string(const QString& str) {
    using algo = contracts::GenerationAlgorithm;
    if (str == "Aldous-Broder") return algo::Aldous_Broder;
    if (str == "Kruskal") return algo::Kruskal;
    if (str == "Prim") return algo::Prim;
    throw std::runtime_error("Can't find algorithm");
}

QString generation_algo_to_string(contracts::GenerationAlgorithm algorithm) {
    using algo = contracts::GenerationAlgorithm;
    switch (algorithm) {
        case algo::Aldous_Broder: return "Aldous-Broder";
        case algo::Kruskal: return "Kruskal";
        case algo::Prim: return "Prim";
        default: return "Unknown";
    }
}

contracts::SolutionAlgorithm solution_algo_for_string(const QString& str) {
    using algo = contracts::SolutionAlgorithm;
    if (str == "Dead End") return algo::DeadEnd;
    if (str == "Wall Follower") return algo::WallFollower;
    if (str == "Dijkstra") return algo::Dijkstra;
    throw std::runtime_error("Can't find algorithm");
}

QString solution_algo_to_string(contracts::SolutionAlgorithm algorithm) {
    using algo = contracts::SolutionAlgorithm;
    switch (algorithm) {
        case algo::Dijkstra: return "Dijkstra";
        case algo::DeadEnd: return "Dead End";
        case algo::WallFollower: return "Wall Follower";
        default: return "Unknown";
    }
}

}

MainWindow::MainWindow(std::shared_ptr<pizza::contract_request_pool> pool,
    QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , request_pool_(std::move(pool))
  , thread_pool_(4)
  , adapter_(*request_pool_, thread_pool_, this)
{
    ui->setupUi(this);
    initSignals();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::launchTask()
{
    if (tasks_remaining_ > 0) {
        QMessageBox::critical(this, "Ошибка", "Задание уже выполняется");
        return;
    }
    tasks_remaining_ = ui->spinBoxCount->value();
    last_count_ = tasks_remaining_;
    time_start_ = QDateTime::currentDateTime();
    for (int i = 0; i < tasks_remaining_; ++i) {
        int row = ui->tableProgress->rowCount();
        id_to_row_.emplace(current_id_, row);

        std::size_t width = ui->spinBoxWidth->value();
        std::size_t height = ui->spinBoxHeight->value();

        contracts::GenerationAlgorithm gen_algo = generation_algo_for_string(ui->comboBoxGenAlgo->currentText());
        contracts::SolutionAlgorithm solve_algo = solution_algo_for_string(ui->comboBoxSolveAlgo->currentText());

        std::size_t inX = ui->spinBoxInX->value();
        std::size_t inY = ui->spinBoxInY->value();
        std::size_t outX = ui->spinBoxOutX->value();
        std::size_t outY = ui->spinBoxOutY->value();
        std::size_t timeout_gen = ui->spinBoxTimeoutGenerate->value();
        std::size_t timeout_solve = ui->spinBoxTimeoutSolve->value();
        async_maze_adapter::task task;
        auto header = task.gen_request.mutable_header();
        header->set_entrance_x(inX);
        header->set_entrance_y(inY);
        header->set_exit_x(outX);
        header->set_exit_y(outY);
        header->set_id(current_id_);
        header->set_width(width);
        header->set_height(height);

        task.gen_request.set_algorithm(gen_algo);
        task.info.first = solve_algo;
        task.info.second.first = timeout_gen;
        task.info.second.second = timeout_solve;

        ui->tableProgress->insertRow(row);
        ui->tableProgress->setItem(row, 0, new QTableWidgetItem(QString::number(current_id_)));
        ui->tableProgress->setItem(row, 1, new QTableWidgetItem(QString::number(width)));
        ui->tableProgress->setItem(row, 2, new QTableWidgetItem(QString::number(height)));
        ui->tableProgress->setItem(row, 3, new QTableWidgetItem("Отправлено на генерацию"));
        ui->tableProgress->setItem(row, 4, new QTableWidgetItem("-"));
        ui->tableProgress->setItem(row, 5, new QTableWidgetItem("-"));
        ui->tableProgress->setItem(row, 6, new QTableWidgetItem(generation_algo_to_string(gen_algo)));
        ui->tableProgress->setItem(row, 7, new QTableWidgetItem(solution_algo_to_string(solve_algo)));
        adapter_.launch_task(std::move(task));
        ++current_id_;
    }
}

void MainWindow::on_generator_response(contracts::MazeHeader header, uint32_t elapsed_ms)
{
    set_status(header.id(), "Сохранение изображения ген. лабиринта");
    int row = id_to_row_.at(header.id());
    ui->tableProgress->item(row, 4)->setText(QString::number(elapsed_ms));
}

void MainWindow::on_generator_exception(std::size_t id, QString exception)
{
    set_status(id, "Таймаут генерации, переотправка...");
}

void MainWindow::on_generator_image_saved(std::size_t id)
{
    set_status(id, "Отправлено на решение");
}

void MainWindow::on_solver_response(contracts::MazeHeader header, uint32_t elapsed_ms)
{
    set_status(header.id(), "Сохранение изображения реш. лабиринта");
    int row = id_to_row_.at(header.id());
    ui->tableProgress->item(row, 5)->setText(QString::number(elapsed_ms));
}

void MainWindow::on_solver_exception(std::size_t id, QString exception)
{
    set_status(id, "Таймаут решения, переотправка...");
}

void MainWindow::on_solver_image_saved(std::size_t id)
{
    set_status(id, "Завершено");
    --tasks_remaining_;
    if (tasks_remaining_ == 0) {
        auto elapsed = time_start_.msecsTo(QDateTime::currentDateTime());
        QMessageBox::information(this, "Завершено", QString("Генерация и решение %1 лабиринтов\nзавершено за %2 милисекунд").arg(last_count_).arg(elapsed));
    }
}

void MainWindow::initSignals()
{
    setInOutMaxValues(ui->spinBoxHeight->value(), ui->spinBoxWidth->value());
    connect(ui->spinBoxHeight, &QSpinBox::editingFinished, [this]() {
        int value = ui->spinBoxHeight->value();
        if (value % 2 == 0) ui->spinBoxHeight->setValue(--value);
        setInOutMaxValues(value, ui->spinBoxWidth->value());
    });

    connect(ui->spinBoxWidth, &QSpinBox::editingFinished, [this]() {
        int value = ui->spinBoxWidth->value();
        if (value % 2 == 0) ui->spinBoxWidth->setValue(--value);
        setInOutMaxValues(ui->spinBoxHeight->value(), value);
    });

    connect(ui->btnLaunchTask, &QPushButton::clicked, this, &MainWindow::launchTask);

    connect(&adapter_, &async_maze_adapter::generator_response_received, this, &MainWindow::on_generator_response);
    connect(&adapter_, &async_maze_adapter::generator_exception_received, this, &MainWindow::on_generator_exception);
    connect(&adapter_, &async_maze_adapter::generator_image_saved, this, &MainWindow::on_generator_image_saved);
    connect(&adapter_, &async_maze_adapter::solver_response_received, this, &MainWindow::on_solver_response);
    connect(&adapter_, &async_maze_adapter::solver_exception_received, this, &MainWindow::on_solver_exception);
    connect(&adapter_, &async_maze_adapter::solver_image_saved, this, &MainWindow::on_solver_image_saved);
}

void MainWindow::set_status(std::size_t id, const QString &status)
{
    int row = id_to_row_.at(id);
    ui->tableProgress->item(row, 3)->setText(status);
}

void MainWindow::setInOutMaxValues(int maxHeight, int maxWidth)
{
    ui->spinBoxInX->setMaximum(maxWidth - 2);
    ui->spinBoxInY->setMaximum(maxHeight - 1);
    ui->spinBoxOutX->setMaximum(maxWidth - 2);
    ui->spinBoxOutY->setMaximum(maxHeight - 1);
}
