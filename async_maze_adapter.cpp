#include "async_maze_adapter.hpp"

#include "pizza/contracts/AnyMessage.pb.h"

namespace {

struct generator_contract {
    using request_type = contracts::MazeGeneratorRequest;
    using response_type = contracts::MazeGeneratorResponse;
};

struct solver_contract {
    using request_type = contracts::MazeSolverRequest;
    using response_type = contracts::MazeSolverResponse;
};

QString exception_to_string(std::exception_ptr ex_ptr) {
    try {
        std::rethrow_exception(ex_ptr);
    } catch (const std::exception& ex) {
        return ex.what();
    } catch (...) {
        return "Unknown error";
    }
}

}

async_maze_adapter::async_maze_adapter(pizza::contract_request_pool &req_pool,
    boost::asio::thread_pool &thread_pool, QObject *parent)
    : QObject(parent)
    , request_pool_(req_pool)
    , thread_pool_(thread_pool)
    , storage_(thread_pool)
{

}

void async_maze_adapter::launch_task(task t)
{
    std::size_t id = t.gen_request.header().id();
    gen_requests_.emplace(id, std::move(t.gen_request));
    info_.emplace(id, t.info);
    send_generation_task(id);
}

void async_maze_adapter::send_generation_task(std::size_t id)
{
    auto save_image_callback = [this, id] {
        QMetaObject::invokeMethod(this, [this, id] {
            emit generator_image_saved(id);
        });
        send_solution_task(id);
    };
    auto response_callback = [this, id, save_image_callback](generator_contract::response_type response, std::exception_ptr ex_ptr) {
        if (ex_ptr || response.has_error()) {
            QString error = ex_ptr ? exception_to_string(ex_ptr) : QString::fromStdString(response.error());
            QMetaObject::invokeMethod(this, [this, id, error = std::move(error)] {
                emit generator_exception_received(id, std::move(error));
            });
            send_generation_task(id);
        } else {
            QMetaObject::invokeMethod(this, [this, hdr = response.header(), t = response.elapsed_ms()] {
                emit generator_response_received(std::move(hdr), t);
            });
            gen_requests_.erase(id);
            storage_.insert_and_save_generated(id, std::move(response), save_image_callback);
        }
    };
    auto req_timeout = std::chrono::seconds(info_.at(id).second.first);
    request_pool_.async_request<pizza::AnyMessage, generator_contract>("maze_generator", gen_requests_[id], response_callback, req_timeout);
}

void async_maze_adapter::send_solution_task(std::size_t id)
{
    contracts::MazeSolverRequest request;
    std::chrono::seconds req_timeout;
    {
        std::lock_guard<std::mutex> _(storage_.lock());
        req_timeout = std::chrono::seconds(info_.at(id).second.second);
        const auto& stored = storage_.storage().at(id);
        request.mutable_header()->CopyFrom(stored.generated.header());
        request.set_maze(stored.generated.maze());
        request.set_algorithm(info_.at(id).first);
    }
    auto save_image_callback = [this, id] {
        QMetaObject::invokeMethod(this, [this, id] {
            emit solver_image_saved(id);
        });
    };
    auto response_callback = [this, id, save_image_callback](solver_contract::response_type response, std::exception_ptr ex_ptr) {
        if (ex_ptr || response.has_error()) {
            QString error = ex_ptr ? exception_to_string(ex_ptr) : QString::fromStdString(response.error());
            QMetaObject::invokeMethod(this, [this, id, error = std::move(error)] {
                emit solver_exception_received(id, error);
            });
            send_solution_task(id);
        } else {
            QMetaObject::invokeMethod(this, [this, hdr = response.header(), t = response.elapsed_ms()] {
                emit solver_response_received(std::move(hdr), t);
            });
            info_.erase(id);
            storage_.insert_and_save_solved(id, std::move(response), save_image_callback);
        }
    };
    request_pool_.async_request<pizza::AnyMessage, solver_contract>("maze_solver", request, response_callback, req_timeout);
}
