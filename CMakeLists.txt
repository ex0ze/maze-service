project(maze LANGUAGES CXX)
cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

option(BuildGenerator "BuildGenerator" ON)
option(BuildSolver "BuildSolver" ON)
option(BuildClient "BuildClient" ON)

if (BuildClient)
    set(CMAKE_AUTOUIC ON)
    set(CMAKE_AUTOMOC ON)
    set(CMAKE_AUTORCC ON)
endif()

find_package(Threads)
find_package(Protobuf REQUIRED)

option(PIZZA_STATIC "Use static linkage" ON)

add_subdirectory(pizza)

generate_protobuf(${CMAKE_CURRENT_SOURCE_DIR}/proto ProtoHeaders ProtoSources)


set(MAZE_HEADERS
    maze/generation/aldous_broder.h
    maze/generation/generator_factory.h
    maze/generation/kruskal.h
    maze/generation/maze_generator.h
    maze/generation/prim.h
    maze/serialization/serialization.h
    maze/solution/dead_end_solver.h
    maze/solution/dijkstra_solver.h
    maze/solution/solver_factory.h
    maze/solution/solver.h
    maze/solution/wall_follower_solver.h)

set(MAZE_SOURCES
    maze/generation/aldous_broder.cpp
    maze/generation/generator_factory.cpp
    maze/generation/kruskal.cpp
    maze/generation/maze_generator.cpp
    maze/generation/prim.cpp
    maze/serialization/serialization.cpp
    maze/solution/dead_end_solver.cpp
    maze/solution/dijkstra_solver.cpp
    maze/solution/solver_factory.cpp
    maze/solution/solver.cpp
    maze/solution/wall_follower_solver.cpp)

if (BuildGenerator)
    set(MAZE_GENERATOR_SOURCES
        maze_generator.cpp
        ${ProtoSources}
        ${MAZE_SOURCES})
    
    set(MAZE_GENERATOR_HEADERS
        ${ProtoHeaders}
        ${MAZE_HEADERS})
    
    add_executable(maze_generator ${MAZE_GENERATOR_SOURCES} ${MAZE_GENERATOR_HEADERS})
    target_link_libraries(maze_generator PUBLIC pizza ${PIZZA_DEPEND_LIBS} Threads::Threads)
endif()

if (BuildSolver)
    set(MAZE_SOLVER_SOURCES
        maze_solver.cpp
        ${ProtoSources}
        ${MAZE_SOURCES})
    
    set(MAZE_SOLVER_HEADERS
        ${ProtoHeaders}
        ${MAZE_HEADERS})
    
    add_executable(maze_solver ${MAZE_SOLVER_SOURCES} ${MAZE_SOLVER_HEADERS})
    target_link_libraries(maze_solver PUBLIC pizza ${PIZZA_DEPEND_LIBS} Threads::Threads)
endif()

if (BuildClient)
    find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets REQUIRED)
    find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets REQUIRED)
    find_package(OpenCV REQUIRED)
    
    set(MAZE_CLIENT_SOURCES
        async_maze_adapter.cpp
        client_storage.cpp
        maze_client.cpp
        mainwindow.cpp
        ${ProtoSources}
        ${MAZE_SOURCES})
    
    set(MAZE_CLIENT_HEADERS
        async_maze_adapter.hpp
        client_storage.hpp
        mainwindow.hpp
        mainwindow.ui
        maze/image/image.hpp
        ${ProtoHeaders}
        ${MAZE_HEADERS})
    
    add_executable(maze_client ${MAZE_CLIENT_SOURCES} ${MAZE_CLIENT_HEADERS})
    target_link_libraries(maze_client PUBLIC pizza
        ${PIZZA_DEPEND_LIBS}
        Threads::Threads
        Qt${QT_VERSION_MAJOR}::Widgets
        ${OpenCV_LIBS})
    
    target_include_directories(maze_client PUBLIC ${OpenCV_INCLUDE_DIRS})
endif()
