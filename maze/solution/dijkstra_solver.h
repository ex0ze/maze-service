/**
 * @file    dijkstra_solver.h
 * @author  Ferenc Nemeth
 * @date    8 Jan 2019
 * @brief   Solving algorithms for mazes.
 *
 *          Copyright (c) 2019 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#pragma once

#include "solver.h"

namespace maze
{

class dijkstra_solver : public solver 
{
public:
    void solve(std::vector<std::vector<maze_generator::cell_type>> &vect,
        std::size_t entrance_y, std::size_t entrance_x,
        std::size_t exit_y, std::size_t exit_x) override;
};

} // namespace maze
