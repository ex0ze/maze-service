/**
 * @file    dead_end_solver.cpp
 * @author  Ferenc Nemeth
 * @date    8 Jan 2019
 * @brief   Solving algorithms for mazes.
 *
 *          Copyright (c) 2019 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "dead_end_solver.h"

void maze::dead_end_solver::solve(std::vector<std::vector<maze_generator::cell_type>> &vect,
    std::size_t entrance_y, std::size_t entrance_x,
    std::size_t exit_y, std::size_t exit_x)
{
    error_check(vect, entrance_y, entrance_x, exit_y, exit_x);

    /* Separate the entrace and exit from everything. */
    vect[entrance_y][entrance_x] = maze_generator::never_dead;
    vect[exit_y][exit_x] = maze_generator::never_dead;
    bool found_dead_end = true;

    /* Loop until there are dead-ends. */
    while (found_dead_end)
    {
        found_dead_end = false;
        /* Loop through the maze. */
        for (std::size_t y = 0; y < vect.size(); y++)
        {
            for (std::size_t x = 0; x < vect[y].size(); x++)
            {
                uint32_t dead_end_counter = 0;

                if (maze_generator::hole == vect[y][x])
                {
                    /* Check the 4 directions of the hole. */
                    /* North. */
                    if ((y > 0) && ((maze_generator::wall == vect[y - 1][x]) || (maze_generator::dead == vect[y - 1][x])))
                    {
                        dead_end_counter++;
                    }
                    /* South. */
                    if (((y + 1) < vect.size()) && ((maze_generator::wall == vect[y + 1][x]) || (maze_generator::dead == vect[y + 1][x])))
                    {
                        dead_end_counter++;
                    }
                    /* West. */
                    if ((x > 0) && ((maze_generator::wall == vect[y][x - 1]) || (maze_generator::dead == vect[y][x - 1])))
                    {
                        dead_end_counter++;
                    }
                    /* East. */
                    if (((x + 1) < vect[0].size()) && ((maze_generator::wall == vect[y][x + 1]) || (maze_generator::dead == vect[y][x + 1])))
                    {
                        dead_end_counter++;
                    }

                    /* If a hole has 3 walls (or dead-ends) next to it, then it is a dead-end. */
                    /* The boundaries are dead-ends in every case. */
                    if ((3 == dead_end_counter) || ((0 == y) || (0 == x) || (vect.size() - 1 == y) || (vect[0].size() - 1 == x)))
                    {
                        vect[y][x] = maze_generator::dead;
                        found_dead_end = true;
                    }
                }
            }
        }
    }

    /* Clean-up. Turn every hole (+ the separted entrance and exit) into a solution and turn back every dead-end into a hole. */
    for (std::size_t y = 0; y < vect.size(); y++)
    {
        for (std::size_t x = 0; x < vect[y].size(); x++)
        {
            if ((maze_generator::hole == vect[y][x]) || (maze_generator::never_dead == vect[y][x]))
            {
                vect[y][x] = maze_generator::solution;
            }
            else if (maze_generator::dead == vect[y][x])
            {
                vect[y][x] = maze_generator::hole;
            }
            else
            {
                /* Do nothing. */
            }
        }
    }
}
