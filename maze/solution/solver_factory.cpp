/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "solver_factory.h"

#include "dead_end_solver.h"
#include "dijkstra_solver.h"
#include "wall_follower_solver.h"

namespace maze
{

std::unique_ptr<solver> make_solver(contracts::SolutionAlgorithm algorithm)
{
    using algo = contracts::SolutionAlgorithm;
    switch (algorithm) {
        case algo::DeadEnd: return std::make_unique<dead_end_solver>();
        case algo::Dijkstra : return std::make_unique<dead_end_solver>();
        case algo::WallFollower : return std::make_unique<wall_follower_solver>();
        default: throw std::runtime_error("Solver does not exist");
    }
}

}
