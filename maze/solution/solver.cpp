/**
 * @file    solver.cpp
 * @author  Ferenc Nemeth
 * @date    8 Jan 2019
 * @brief   Solving algorithms for mazes.
 *
 *          Copyright (c) 2019 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "solver.h"

/**
 * @brief   Error handler for the solver algorithms. It shall be insterted into every member function.
 * @param   &vect       - The vector-vector of the maze we want to solve. It overwrites the input one.
 * @param   entrance_y  - Y coordinate of the entrance.
 * @param   entrance_x  - X coordinate of the entrance.
 * @param   exit_y      - Y coordinate of the exit.
 * @param   exit_x      - X coordinate of the exit.
 * @return  void
 */
void maze::solver::error_check(const std::vector<std::vector<maze_generator::cell_type>> &vect,
                               std::size_t entrance_y, std::size_t entrance_x,
                               std::size_t exit_y, std::size_t exit_x)
{
    if ((vect.size() <= entrance_y) || (vect[0].size() <= entrance_x) || (vect.size() <= exit_y) || (vect[0].size() <= exit_x))
    {
        throw std::invalid_argument("Out of boundary!");
    }

    if ((maze_generator::hole != vect[entrance_y][entrance_x]) || (maze_generator::hole != vect[entrance_y][entrance_x]))
    {
        throw std::invalid_argument("The entrance and exit must be holes (0).");
    }
}
