/**
 * @file    solver.h
 * @author  Ferenc Nemeth
 * @date    8 Jan 2019
 * @brief   Solving algorithms for mazes.
 *
 *          Copyright (c) 2019 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#pragma once

#include <vector>
#include <random>
#include <algorithm>
#include "../generation/maze_generator.h"

namespace maze
{

class solver
{
public:
    virtual void solve(std::vector<std::vector<maze_generator::cell_type>> &vect,
        std::size_t entrance_y, std::size_t entrance_x,
        std::size_t exit_y, std::size_t exit_x) = 0;

protected:

    void error_check(const std::vector<std::vector<maze_generator::cell_type>> &vect,
                     std::size_t entrance_y, std::size_t entrance_x,
                     std::size_t exit_y, std::size_t exit_x);
};

} // namespace maze
