/**
 * @file    dijkstra_solver.cpp
 * @author  Ferenc Nemeth
 * @date    8 Jan 2019
 * @brief   Solving algorithms for mazes.
 *
 *          Copyright (c) 2019 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "dijkstra_solver.h"

void maze::dijkstra_solver::solve(std::vector<std::vector<maze_generator::cell_type>> &vect,
    std::size_t entrance_y, std::size_t entrance_x,
    std::size_t exit_y, std::size_t exit_x)
{
    struct distance
    {
        std::size_t y;
        std::size_t x;
    };
    error_check(vect, entrance_y, entrance_x, exit_y, exit_x);

    std::vector<distance> distances;
    uint32_t distance_cnt = 3; /* Should be 0, but 0-2 are already used, so it would confuse everything. */
    distances.push_back({entrance_y, entrance_x});
    bool new_distance = true;
    std::size_t y = 0;
    std::size_t x = 0;

    /* Walk away from the entrace and save their distance (from the entrance). */
    while (new_distance)
    {
        new_distance = false;
        std::size_t distance_max = distances.size();
        distance_cnt++;
        /* With the for loop, we can walk "parellel". */
        /* If there are 2 path, then there'll 2 elements in the vector, if there are 3, then 3, etc..*/
        for (std::size_t i = 0; i < distance_max; i++)
        {
            y = distances[0].y;
            x = distances[0].x;

            vect[y][x] = static_cast<maze_generator::cell_type>(distance_cnt);
            /* if north is a hole, then save. */
            if ((y > 0) && (maze_generator::hole == vect[y - 1][x]))
            {
                distances.push_back({y - 1, x});
                new_distance = true;
            }
            /* Ff south is a hole, then save. */
            if (((y + 1) < vect.size()) && (maze_generator::hole == vect[y + 1][x]))
            {
                distances.push_back({y + 1, x});
                new_distance = true;
            }
            /* if west is a hole, then save. */
            if ((x > 0) && (maze_generator::hole == vect[y][x - 1]))
            {
                distances.push_back({y, x - 1});
                new_distance = true;
            }
            /* If east is a hole, then save. */
            if (((x + 1) < vect[0].size()) && (maze_generator::hole == vect[y][x + 1]))
            {
                distances.push_back({y, x + 1});
                new_distance = true;
            }

            /* Stop at the end. It could run and check every cell in the maze, but it would be waste of time. */
            if ((y == exit_y) && (x == exit_x))
            {
                new_distance = false;
                break;
            }

            distances.erase(distances.begin());
        }
    }

    /* Walk back from the exit to the entrance. */
    y = exit_y;
    x = exit_x;
    distance_cnt = vect[y][x];

    /* Loop until we aren't at the beginning. */
    while (3 != distance_cnt)
    {
        /* Mark everything as a solution on the way. */
        vect[y][x] = maze_generator::solution;
        distance_cnt--;
        if ((y > 0) && (distance_cnt == vect[y - 1][x]))
        {
            y--;
        }
        else if (((y + 1) < vect.size()) && (distance_cnt == vect[y + 1][x]))
        {
            y++;
        }
        else if ((x > 0) && (distance_cnt == vect[y][x - 1]))
        {
            x--;
        }
        else if (((x + 1) < vect[0].size()) && (distance_cnt == vect[y][x + 1]))
        {
            x++;
        }
        else
        {
            /* Do nothing. */
        }
    }

    /* Clean up, the output shall only contain walls, holes or solutions. */
    for (std::size_t y = 0; y < vect.size(); y++)
    {
        for (std::size_t x = 0; x < vect[y].size(); x++)
        {
            if ((maze_generator::wall != vect[y][x]) && (maze_generator::solution != vect[y][x]))
            {
                vect[y][x] = maze_generator::hole;
            }
        }
    }
}
