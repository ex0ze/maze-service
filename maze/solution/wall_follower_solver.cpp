/**
 * @file    wall_follower_solver.cpp
 * @author  Ferenc Nemeth
 * @date    8 Jan 2019
 * @brief   Solving algorithms for mazes.
 *
 *          Copyright (c) 2019 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "wall_follower_solver.h"

void maze::wall_follower_solver::solve(std::vector<std::vector<maze_generator::cell_type>> &vect,
                                       std::size_t entrance_y, std::size_t entrance_x,
                                       std::size_t exit_y, std::size_t exit_x)
{
    enum follow_rule : uint8_t
    {
        left = 0,
        right = 1
    };
    error_check(vect, entrance_y, entrance_x, exit_y, exit_x);
    follow_rule rule = left;

    struct element
    {
        std::size_t y;
        std::size_t x;
        maze_generator::direction dir;
    };

    std::size_t y = entrance_y;
    std::size_t x = entrance_x;
    maze_generator::direction direction = maze_generator::north;
    std::vector<uint32_t> directions(4);
    std::vector<element> visited;
    visited.push_back({y, x, static_cast<maze_generator::direction>(99)}); /* The first direction has to be invalid, so it never gets removed. */
    vect[y][x] = maze_generator::solution;

    /* Loop until we aren't at the end. */
    while (!((y == exit_y) && (x == exit_x)))
    {

        /* Left-hand rule and its priorities. */
        if (left == rule)
        {
            if (maze_generator::north == direction)
            {
                directions = {maze_generator::west, maze_generator::north, maze_generator::east, maze_generator::south};
            }
            else if (maze_generator::south == direction)
            {
                directions = {maze_generator::east, maze_generator::south, maze_generator::west, maze_generator::north};
            }
            else if (maze_generator::west == direction)
            {
                directions = {maze_generator::south, maze_generator::west, maze_generator::north, maze_generator::east};
            }
            else if (maze_generator::east == direction)
            {
                directions = {maze_generator::north, maze_generator::east, maze_generator::south, maze_generator::west};
            }
            else
            {
                /* Do nothing. */
            }
        }
        /* Right-hand rule and its priorities. */
        else if (right == rule)
        {
            if (maze_generator::north == direction)
            {
                directions = {maze_generator::east, maze_generator::north, maze_generator::west, maze_generator::south};
            }
            else if (maze_generator::south == direction)
            {
                directions = {maze_generator::west, maze_generator::south, maze_generator::east, maze_generator::north};
            }
            else if (maze_generator::west == direction)
            {
                directions = {maze_generator::north, maze_generator::west, maze_generator::south, maze_generator::east};
            }
            else if (maze_generator::east == direction)
            {
                directions = {maze_generator::south, maze_generator::east, maze_generator::north, maze_generator::west};
            }
            else
            {
                /* Do nothing. */
            }
        }
        else
        {
            /* Do nothing. */
        }

        /* Try to move in every direction. */
        /* If it is possible to go there, then go (the directions are in priority order). */
        /* If we haven't been there, then push it to the visited stack and mark as a solution. */
        /* If we have been there, then pop it from the visited stack and remove the soliton mark. */
        for (std::size_t i = 0; i < directions.size(); i++)
        {
            if (maze_generator::north == directions[i])
            {
                if ((y > 0) && (maze_generator::wall != vect[y - 1][x]))
                {
                    std::size_t last = visited.size() - 1;
                    y--;
                    direction = maze_generator::north;
                    if (maze_generator::south == visited[last].dir)
                    {
                        vect[visited[last].y][visited[last].x] = maze_generator::hole;
                        visited.pop_back();
                    }
                    else
                    {
                        visited.push_back({y, x, direction});
                        vect[y][x] = maze_generator::solution;
                    }
                    break;
                }
            }
            else if (maze_generator::south == directions[i])
            {
                if (((y + 1) < vect.size()) && (maze_generator::wall != vect[y + 1][x]))
                {
                    std::size_t last = visited.size() - 1;
                    y++;
                    direction = maze_generator::south;
                    if (maze_generator::north == visited[last].dir)
                    {
                        vect[visited[last].y][visited[last].x] = maze_generator::hole;
                        visited.pop_back();
                    }
                    else
                    {
                        visited.push_back({y, x, direction});
                        vect[y][x] = maze_generator::solution;
                    }
                    break;
                }
            }
            else if (maze_generator::west == directions[i])
            {
                if ((x > 0) && (maze_generator::wall != vect[y][x - 1]))
                {
                    std::size_t last = visited.size() - 1;
                    x--;
                    direction = maze_generator::west;
                    if (maze_generator::east == visited[last].dir)
                    {
                        vect[visited[last].y][visited[last].x] = maze_generator::hole;
                        visited.pop_back();
                    }
                    else
                    {
                        visited.push_back({y, x, direction});
                        vect[y][x] = maze_generator::solution;
                    }
                    break;
                }
            }
            else if (maze_generator::east == directions[i])
            {
                if (((x + 1) < vect[0].size()) && (maze_generator::wall != vect[y][x + 1]))
                {
                    std::size_t last = visited.size() - 1;
                    x++;
                    direction = maze_generator::east;
                    if (maze_generator::west == visited[last].dir)
                    {
                        vect[visited[last].y][visited[last].x] = maze_generator::hole;
                        visited.pop_back();
                    }
                    else
                    {
                        visited.push_back({y, x, direction});
                        vect[y][x] = maze_generator::solution;
                    }
                    break;
                }
            }
            else
            {
                /* Do nothing. */
            }
        }
    }
}
