/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "serialization.h"

namespace maze
{

namespace {

bool set_cell(maze_generator::area_type& area,
    std::size_t& curr_height, std::size_t& curr_width, 
    std::size_t height, std::size_t width,
    maze_generator::cell_type cell)
{
    area.at(curr_height).at(curr_width) = cell;
    ++curr_width;
    if (curr_width == width) {
        curr_width = 0;
        ++curr_height;
    }
    if (curr_height == height) return true;
    return false;
}

} // anonymous namespace

std::string to_bytes(const maze_generator::area_type& area, serialization_type type)
{
    using cell_type = maze_generator::cell_type;
    cell_type comp = (type == serialization_type::unsolved) ?
        cell_type::hole : cell_type::solution;

    std::size_t num_bits = area.size() * area.at(0).size();
    std::size_t num_bytes = num_bits / 8;
    if (num_bytes * 8 < num_bits)
        ++num_bytes;
    std::string bytes(num_bytes, 0);
    std::size_t current_byte = 0;
    uint8_t current_bit = 0;
    auto set_bit = [&](cell_type val) {
        char& offset = bytes[current_byte];
        if (val == comp) {
            offset |= (static_cast<uint8_t>(1) << current_bit);
        }
        ++current_bit;
        if (current_bit == 8) {
            ++current_byte;
            current_bit = 0;
        }
    };

    for (const auto& row : area) {
        for (auto cell : row) {
            set_bit(cell);
        }
    }
    return bytes;
}

maze_generator::area_type unsolved_from_bytes(const std::string& bytes,
    std::size_t height, std::size_t width)
{
    using cell_type = maze_generator::cell_type;

    maze_generator::area_type result(height, std::vector<cell_type>(width, cell_type::wall));
    std::size_t current_height = 0, current_width = 0;

    for (char b : bytes) {
        for (uint8_t i = 0; i < 8; ++i) {
            uint8_t mask = (1 << i) & 0xFF;
            if (set_cell(result, current_height, current_width,
                height, width,
                (b & mask) ? cell_type::hole : cell_type::wall))
                return result;
        }
    }
    return result;
}

void solved_from_bytes(const std::string& bytes, std::size_t height, std::size_t width, maze_generator::area_type& area)
{
    using cell_type = maze_generator::cell_type;
    std::size_t current_height = 0, current_width = 0;

    for (char b : bytes) {
        for (uint8_t i = 0; i < 8; ++i) {
            uint8_t mask = (1 << i) & 0xFF;
            if (set_cell(area,
                current_height, current_width,
                height, width,
                (b & mask) ? cell_type::solution :
                             area.at(current_height).at(current_width)) )
                return;
        }
    }

}

} // namespace maze
