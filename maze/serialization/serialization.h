/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>

#include "../generation/maze_generator.h"

namespace maze
{

enum class serialization_type {
    unsolved,  // for serialization of unsolved maze
    solved     // for serialization of solved maze
};

std::string to_bytes(const maze_generator::area_type& area, serialization_type type);

maze_generator::area_type unsolved_from_bytes(const std::string& bytes, std::size_t height, std::size_t width);
void solved_from_bytes(const std::string& bytes, std::size_t height, std::size_t width, maze_generator::area_type& unsolved);

}
