/**
 * @file    kruskal.cpp
 * @author  Ferenc Nemeth
 * @date    23 Nov 2018
 * @brief   Maze generator class with Kruskal's algorithm.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "kruskal.h"

/**
 * @brief   This method generates the maze with Kruskal's algorithm.
 * @param   void
 * @return  void
 */
void maze::kruskal::generate()
{
    struct element
    {
        std::size_t y;
        std::size_t x;
        orientation orient;
    };
    /* Mersenne Twister 19937 pseudo-random generator. */
    std::mt19937 random_generator(random_device());

    /* Do the initialization for sets, similar way to area. */
    sets.resize(area.size());
    for (std::size_t y = 0; y < area.size(); y++)
    {
        sets[y].resize(area[y].size());
    }

    /* Save every y,x coordinate with a possible movement (vertical or horizontal). */
    /* Also, fill the sets with different values. */
    std::vector<element> elements;
    std::size_t i = 1u;
    for (std::size_t y = 1u; y < (sets.size() - 1); y += 2)
    {
        for (std::size_t x = 1; x < (sets[0].size() - 1); x += 2)
        {
            if ((y + 2) < (sets.size() - 1))
            {
                elements.push_back({y, x, vertical});
            }
            if ((x + 2) < (sets[0].size() - 1))
            {
                elements.push_back({y, x, horizontal});
            }
            sets[y][x] = i;
            i++;
        }
    }

    /* Shuffle the elements vector. */
    std::shuffle(elements.begin(), elements.end(), random_generator);

    /* Do it until there are no elements left. */
    while (elements.size())
    {
        std::size_t y = elements[elements.size() - 1].y;
        std::size_t x = elements[elements.size() - 1].x;
        orientation orientation = elements[elements.size() - 1].orient;
        elements.pop_back();

        if (horizontal == orientation)
        {
            /* If the two sets are different, then make them the same and crave a passage. */
            if (sets[y][x + 2] != sets[y][x])
            {
                replace(sets[y][x + 2u], sets[y][x]);
                for (std::size_t j = 0; j < 3; j++)
                {
                    area[y][x + j] = hole;
                }
            }
        }
        else if (vertical == orientation)
        {
            /* If the two sets are different, then make them the same and crave a passage. */
            if (sets[y + 2][x] != sets[y][x])
            {
                replace(sets[y + 2][x], sets[y][x]);
                for (std::size_t j = 0; j < 3; j++)
                {
                    area[y + j][x] = hole;
                }
            }
        }
        else
        {
            /* Do nothing. */
        }
    }
}

/**
 * @brief   Loops through the sets array and replaces the chosen elements.
 * @param   set_to replace - The type of set, that needs to replaced.
 * @param   sample_set     - The new value, that's going to overwrite the others.
 * @return  void
 */
void maze::kruskal::replace(std::size_t set_to_replace, std::size_t sample_set)
{
    for (std::size_t search_y = 1; search_y < (sets.size() - 1); search_y += 2)
    {
        for (std::size_t search_x = 1; search_x < (sets[0].size() - 1); search_x += 2)
        {
            if (sets[search_y][search_x] == set_to_replace)
            {
                sets[search_y][search_x] = sample_set;
            }
        }
    }
}
