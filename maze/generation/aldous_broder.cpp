/**
 * @file    aldous_broder.cpp
 * @author  Ferenc Nemeth
 * @date    20 Nov 2018
 * @brief   Maze generator class with Aldous-Broder algorithm.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "aldous_broder.h"

/**
 * @brief   This method generates the maze with Aldous-Broder algorithm.
 * @param   void
 * @return  void
 */
void maze::aldous_broder::generate()
{
    std::size_t y = 0;
    std::size_t x = 0;
    /* The number of the cells, that can be visited. */
    std::size_t total_cells = (area.size() / 2) * (area[0].size() / 2);

    /* Mersenne Twister 19937 pseudo-random generator. */
    std::mt19937 random_generator(random_device());
    /* Random starting point. */
    std::uniform_int_distribution<std::size_t> random_start_y(1, area.size() - 2);
    std::uniform_int_distribution<std::size_t> random_start_x(1, area[0].size() - 2);
    /* Random direction. */
    std::uniform_int_distribution<uint8_t> random_dir(static_cast<uint8_t>(north), static_cast<uint8_t>(east));

    /* Make sure, that the two random numbers are odd. */
    y = (random_start_y(random_generator) / 2 * 2 + 1);
    x = (random_start_x(random_generator) / 2 * 2 + 1);

    area[y][x] = hole;
    total_cells--;

    /* Loop until there are no cells left. */
    while (total_cells)
    {
        /* Randomly select a direction to move. */
        direction next_cell = static_cast<direction>(random_dir(random_generator));

        if (north == next_cell)
        {
            /* Check if it is possible to go north. */
            if (y >= 3)
            {
                /* Save the new position. */
                y -= 2;
                /* In case the cell hasn't been visited, then change it to hole and lower the total_cell counter. */
                if (wall == area[y][x])
                {
                    total_cells--;
                    area[y][x] = hole;
                    area[y + 1][x] = hole;
                }
            }
        }
        else if (south == next_cell)
        {
            /* Check if it is possible to go south. */
            if ((y + 2) <= area.size() - 2)
            {
                /* Save the new position. */
                y += 2;
                /* In case the cell hasn't been visited, then change it to hole and lower the total_cell counter. */
                if (wall == area[y][x])
                {
                    total_cells--;
                    area[y][x] = hole;
                    area[y - 1][x] = hole;
                }
            }
        }
        else if (west == next_cell)
        {
            /* Check if it is possible to go west. */
            if (x >= 3)
            {
                /* Save the new position. */
                x -= 2;
                /* In case the cell hasn't been visited, then change it to hole and lower the total_cell counter. */
                if (wall == area[y][x])
                {
                    total_cells--;
                    area[y][x] = hole;
                    area[y][x + 1] = hole;
                }
            }
        }
        else if (east == next_cell)
        {
            /* Check if it is possible to go east. */
            if ((x + 2) <= area[0].size() - 2)
            {
                /* Save the new position. */
                x += 2;
                /* In case the cell hasn't been visited, then change it to hole and lower the total_cell counter. */
                if (wall == area[y][x])
                {
                    total_cells--;
                    area[y][x] = hole;
                    area[y][x - 1] = hole;
                }
            }
        }
        else
        {
            /* Do nothing. */
        }
    }
}
