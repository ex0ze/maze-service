/**
 * @file    prim.cpp
 * @author  Ferenc Nemeth
 * @date    19 Nov 2018
 * @brief   Maze generator class with Prim's algorithm.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "prim.h"

/**
 * @brief   This method generates the maze with Prim's algorithm.
 * @param   void
 * @return  void
 */
void maze::prim::generate()
{
    /* Mersenne Twister 19937 pseudo-random generator. */
    std::mt19937 random_generator(random_device());
    /* Random starting point. */
    std::uniform_int_distribution<std::size_t> random_start_y(1, area.size() - 2);
    std::uniform_int_distribution<std::size_t> random_start_x(1, area[0].size() - 2);
    /* Random direction. */
    std::uniform_int_distribution<uint8_t> random_dir(static_cast<uint8_t>(north), static_cast<uint8_t>(east));

    /* Make sure, that the two random numbers are odd. */
    mark(random_start_y(random_generator) / 2 * 2 + 1, random_start_x(random_generator) / 2 * 2 + 1);

    /* Loop until there are no frontiers left. */
    while (!frontiers.empty())
    {
        bool possible_to_crave = false;

        /* Randomly select a frontier from the list.*/
        std::uniform_int_distribution<std::size_t> random_frontier(0, frontiers.size() - 1);
        std::size_t next_frontier = random_frontier(random_generator);
        std::size_t y = frontiers[next_frontier].y;
        std::size_t x = frontiers[next_frontier].x;
        /* Remove frontier from the list. */
        frontiers.erase(frontiers.begin() + next_frontier);

        /* Try to move to one direction (loop until we find a possible way to go). */
        while (!possible_to_crave)
        {
            direction dir = static_cast<direction>(random_dir(random_generator));
            if (north == dir)
            {
                /* If it is possible to go north, then crave a hole and stop the loop. */
                if ((y > 2) && (hole == area[y - 2][x]))
                {
                    possible_to_crave = true;
                    area[y - 1][x] = hole;
                }
            }
            else if (south == dir)
            {
                /* If it is possible to go south, then crave a hole and stop the loop. */
                if (((y + 2) < (area.size() - 1)) && (hole == area[y + 2][x]))
                {
                    possible_to_crave = true;
                    area[y + 1][x] = hole;
                }
            }
            else if (west == dir)
            {
                /* If it is possible to go west, then crave a hole and stop the loop. */
                if ((x > 2u) && (hole == area[y][x - 2]))
                {
                    possible_to_crave = true;
                    area[y][x - 1] = hole;
                }
            }
            else if (east == dir)
            {
                /* If it is possible to go east, then crave a hole and stop the loop. */
                if (((x + 2) < (area[0].size() - 1)) && (hole == area[y][x + 2]))
                {
                    possible_to_crave = true;
                    area[y][x + 1] = hole;
                }
            }
        }

        /* Create new frontiers. */
        mark(y, x);
    }
}

/**
 * @brief   Set a hole at [y,x] coordinate and mark every possible neighboor cell as frontier.
 * @param   y - Coordinate of the new hole.
 *          x - Coordinate of the new hole.
 * @return  void
 */
void maze::prim::mark(std::size_t y, std::size_t x)
{
    /* Mark as a hole. */
    area[y][x] = hole;
    /* Save the cell at north as a frontier (if it isn't out of boundary). */
    if ((y >= 3u) && (wall == area[y - 2][x]))
    {
        area[y - 2u][x] = frontier;
        frontiers.push_back({y - 2, x});
    }
    /* Save the cell at south as a frontier (if it isn't out of boundary). */
    if (((y + 2) <= area.size() - 2) && (wall == area[y + 2][x]))
    {
        area[y + 2][x] = frontier;
        frontiers.push_back({y + 2u, x});
    }
    /* Save the cell at west as a frontier (if it isn't out of boundary). */
    if ((x >= 3) && (wall == area[y][x - 2]))
    {
        area[y][x - 2] = frontier;
        frontiers.push_back({y, x - 2});
    }
    /* Save the cell at east as a frontier (if it isn't out of boundary). */
    if (((x + 2) <= area[0u].size() - 2) && (wall == area[y][x + 2]))
    {
        area[y][x + 2] = frontier;
        frontiers.push_back({y, x + 2});
    }
}
