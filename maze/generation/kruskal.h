/**
 * @file    kruskal.h
 * @author  Ferenc Nemeth
 * @date    23 Nov 2018
 * @brief   Maze generator class with Kruskal's algorithm.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#pragma once

#include "maze_generator.h"

namespace maze
{

class kruskal : public maze_generator
{
public:
    using maze_generator::maze_generator;
    void generate() override;

private:
    std::vector<std::vector<std::size_t>> sets;

    void replace(std::size_t set_to_replace, std::size_t sample_set);
};

} // namespace maze
