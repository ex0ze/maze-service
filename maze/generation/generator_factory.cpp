/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "generator_factory.h"

#include "aldous_broder.h"
#include "kruskal.h"
#include "prim.h"

namespace maze
{

std::unique_ptr<maze_generator> make_generator(contracts::GenerationAlgorithm algorithm, 
    std::size_t height, std::size_t width)
{
    using algo = contracts::GenerationAlgorithm;
    switch (algorithm) {
        case algo::Aldous_Broder: return std::make_unique<aldous_broder>(height, width);
        case algo::Kruskal: return std::make_unique<kruskal>(height, width);
        case algo::Prim: return std::make_unique<prim>(height, width);
        default: throw std::runtime_error("Generator does not exist");
    }
}

}
