/**
 * @file    maze_generator.cpp
 * @author  Ferenc Nemeth
 * @date    3 Dec 2018
 * @brief   Base class: methods and variables used by every maze generator.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "maze_generator.h"

/**
 * @brief   Constructor. Generates a 2D vector, which represents the maze.
 * @param   height - Height of the maze.
 * @param   width  - Width of the maze.
 * @return  void
 */
maze::maze_generator::maze_generator(std::size_t height, std::size_t width)
{
    /* Make sure, that the dimensions are odd numbers, otherwise the maze would look strange. */
    if ((!(height % 2)) || (!(width % 2)))
    {
        throw std::invalid_argument("Height and width must be odd numbers!");
    }

    area.resize(height, std::vector<cell_type>(width, wall));
}

maze::maze_generator::maze_generator(maze::maze_generator::area_type area_)
{
    area = std::move(area_);
}

/**
 * @brief   Manually changes the value of a cell.
 * @param   y     - The y coordinate of the cell.
 * @param   x     - The x coordinate of the cell.
 * @param   value - The value, either 0 or 1.
 * @return  void
 */
void maze::maze_generator::set_cell(std::size_t y, std::size_t x, cell_type value)
{
    area.at(y).at(x) = value;
}

/**
 * @brief   Returns the value of a cell.
 * @param   y     - The y coordinate of the hole.
 * @param   x     - The x coordinate of the hole.
 * @return  value - The actual value of the cell, either 0 or 1.
 */
maze::maze_generator::cell_type maze::maze_generator::get_cell(std::size_t y, std::size_t x)
{
    return area.at(y).at(x);
}

/**
 * @brief   Returns the maze.
 * @param   void
 * @return  area - 2D vector of the maze. 1 represents a hole, 0 represents a wall.
 */
maze::maze_generator::area_type &
maze::maze_generator::get_maze() noexcept
{
    return area;
}

const maze::maze_generator::area_type &
maze::maze_generator::get_maze() const noexcept
{
    return area;
}

/**
 * @brief   Overwrites the current maze.
 * @param   vect - 2D vector of the maze. 1 represents a hole, 0 represents a wall.
 * @return  void
 */
void maze::maze_generator::set_maze(maze::maze_generator::area_type vect)
{
    area = std::move(vect);
}

/**
 * @brief   Reshapes the maze.
 * @param   height - New height of the maze.
 * @param   width  - New width of the maze.
 * @return  void
 */
void maze::maze_generator::reshape(std::size_t height, std::size_t width)
{
    /* Make sure, that the dimensions are odd numbers, otherwise the maze would look strange. */
    if ((!(height % 2u)) || (!(width % 2u)))
    {
        throw std::invalid_argument("Height and width must be odd numbers!");
    }

    area.resize(height, std::vector<cell_type>(width, wall));
}

/**
 * @brief   Returns the height of the maze.
 * @param   void
 * @return  height
 */
std::size_t maze::maze_generator::get_height() const
{
    return area.size();
}

/**
 * @brief   Returns the width of the maze.
 * @param   void
 * @return  width
 */
std::size_t maze::maze_generator::get_width() const
{
    return area[0].size();
}
