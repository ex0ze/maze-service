/**
 * @file    prim.h
 * @author  Ferenc Nemeth
 * @date    19 Nov 2018
 * @brief   Maze generator class with Prim's algorithm.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#pragma once

#include "maze_generator.h"

namespace maze
{

class prim : public maze_generator
{
public:
    using maze_generator::maze_generator;
    void generate() override;

private:
    struct frontier_location
    {
        std::size_t y;
        std::size_t x;
    };

    std::vector<frontier_location> frontiers;

    void mark(std::size_t y, std::size_t x);
};

} // namespace maze
