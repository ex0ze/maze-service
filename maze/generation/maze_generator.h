/**
 * @file    maze_generator.h
 * @author  Ferenc Nemeth
 * @date    3 Dec 2018
 * @brief   Base class: methods and variables used by every maze generator.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#pragma once

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <random>
#include <vector>

namespace maze
{

class maze_generator
{
public:
    maze_generator(std::size_t height, std::size_t width);

    enum cell_type : uint32_t
    {
        wall = 0,
        hole = 1,
        solution = 2,
        frontier = 3,  // prim algo
        dead = 4,      // dead_end algo
        never_dead = 5 // dead_end algo
    };
    using area_type = std::vector<std::vector<cell_type>>;

    maze_generator(area_type area);

    enum direction : uint8_t
    {
        north = 0,
        south = 1,
        west = 2,
        east = 3
    };

    void set_cell(std::size_t y, std::size_t x, cell_type value);
    cell_type get_cell(std::size_t y, std::size_t x);

    area_type& get_maze() noexcept;
    const area_type& get_maze() const noexcept;
    void set_maze(area_type vect);

    void reshape(std::size_t new_height, std::size_t new_width);

    std::size_t get_height() const;
    std::size_t get_width() const;

    virtual void generate() = 0;

protected:
    std::random_device random_device;

    enum orientation : uint8_t
    {
        vertical = 0,
        horizontal = 1,
        none // binary_tree
    };

    area_type area;
};

} // namespace maze
