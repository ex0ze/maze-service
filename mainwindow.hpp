#pragma once

#include <QMainWindow>
#include <QDateTime>

#include <boost/asio/thread_pool.hpp>

#include "pizza/contract_request_pool.hpp"
#include "pizza/bus_link.hpp"
#include "client_storage.hpp"
#include <map>

#include "proto/maze.pb.h"
#include "async_maze_adapter.hpp"

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(std::shared_ptr<pizza::contract_request_pool> pool,
        QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void launchTask();
    void on_generator_response(contracts::MazeHeader header, uint32_t elapsed_ms);
    void on_generator_exception(std::size_t id, QString exception);
    void on_generator_image_saved(std::size_t id);
    void on_solver_response(contracts::MazeHeader header, uint32_t elapsed_ms);
    void on_solver_exception(std::size_t id, QString exception);
    void on_solver_image_saved(std::size_t id);

private:
    void setInOutMaxValues(int maxHeight, int maxWidth);
    void initSignals();
    void set_status(std::size_t id, const QString& status);

    Ui::MainWindow *ui;
    std::shared_ptr<pizza::contract_request_pool> request_pool_;
    std::size_t current_id_ = 0;

    boost::asio::thread_pool thread_pool_;

    async_maze_adapter adapter_;

    std::map<std::size_t, int> id_to_row_;

    QDateTime time_start_;
    std::size_t tasks_remaining_ = 0;
    std::size_t last_count_ = 0;
};
