/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

#include <QApplication>
#include <QMessageBox>

#include <boost/program_options.hpp>

#include "pizza/contract_request_pool.hpp"
#include "pizza/make_from_requester.hpp"
#include <pizza-config/from_json.hpp>

#include "proto/maze.pb.h"
#include "maze/generation/aldous_broder.h"
#include "maze/solution/dijkstra_solver.h"
#include "maze/serialization/serialization.h"

#include "mainwindow.hpp"

void print_to_stdout_and_messagebox(std::ostream& out, const std::string& what, const std::string& info, bool error = false) {
    out << what << std::endl;
    out << info << std::endl;
    if (error)
        QMessageBox::critical(nullptr, QString::fromStdString(what), QString::fromStdString(info));
    else
        QMessageBox::information(nullptr, QString::fromStdString(what), QString::fromStdString(info));
}

bool load_request_pool_map(const std::string& path, pizza::request_pool_map& map) {
    try {
        map = pizza::make_request_pool_map(pizza::from_json<pizza::requester>(path));
    }
    catch (const std::exception& ex) {
        print_to_stdout_and_messagebox(std::cerr, "Error", ex.what(), true);
        return false;
    }
    return true;
}
#include <QTextStream>
int main(int argc, char * argv[])
{
    namespace po = boost::program_options;
    QApplication app(argc, argv);
    po::options_description description("Maze client");
    description.add_options()
        ("help,h", "Show this help")
        ("from-json", po::value<std::string>(), "Initialize requester from json");
    po::parsed_options parsed(nullptr, 0);
    try {
        parsed = po::command_line_parser(argc, argv).options(description).allow_unregistered().run();
    }
    catch (const std::exception& e) {
        std::ostringstream stream;
        stream << e.what() << std::endl;
        description.print(stream);
        print_to_stdout_and_messagebox(std::cerr, "Error", stream.str(), true);
        return 1;
    }
    po::variables_map vm;
    po::store(parsed, vm);
    po::notify(vm);
    if (vm.count("help")) {
        std::ostringstream stream;
        description.print(stream);
        print_to_stdout_and_messagebox(std::cout, "Help", stream.str());
        return 0;
    }
    if (!vm.count("from-json")) {
        std::ostringstream stream;
        stream << "Missing --from-json option" << std::endl;
        description.print(stream);
        print_to_stdout_and_messagebox(std::cerr, "Error", stream.str(), true);
        return 1;
    }

    pizza::request_pool_map pool_map;
    pizza::bus_map bus_map;
    if (!load_request_pool_map(vm["from-json"].as<std::string>(), pool_map))
        return 1;
    MainWindow w(pool_map.at("maze_broker"));
    w.show();
    return app.exec();
}
