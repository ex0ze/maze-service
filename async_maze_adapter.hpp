#pragma once

#include <QObject>

#include <boost/asio/thread_pool.hpp>
#include <map>

#include "pizza/contract_request_pool.hpp"
#include "proto/maze.pb.h"
#include "client_storage.hpp"

class async_maze_adapter : public QObject {
    Q_OBJECT
public:
    using timeouts_ms = std::pair<std::size_t, std::size_t>;
    using task_info = std::pair<contracts::SolutionAlgorithm, timeouts_ms>;
    struct task {
        contracts::MazeGeneratorRequest gen_request;
        task_info info;
    };

    explicit async_maze_adapter(pizza::contract_request_pool& req_pool,
        boost::asio::thread_pool& thread_pool,
        QObject *parent = nullptr);
    void launch_task(task t);
    const client_storage& storage() const noexcept { return storage_; }
signals:
    void generator_response_received(contracts::MazeHeader header, uint32_t elapsed_ms);
    void generator_exception_received(std::size_t id, QString exception);
    void generator_image_saved(std::size_t id);
    void solver_response_received(contracts::MazeHeader header, uint32_t elapsed_ms);
    void solver_exception_received(std::size_t id, QString exception);
    void solver_image_saved(std::size_t id);
private:
    void send_generation_task(std::size_t id);
    void send_solution_task(std::size_t id);

    pizza::contract_request_pool& request_pool_;
    boost::asio::thread_pool& thread_pool_;
    client_storage storage_;

    std::map<std::size_t, contracts::MazeGeneratorRequest> gen_requests_;
    std::map<std::size_t, task_info> info_;
};

