/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/asio/thread_pool.hpp>

#include <future>
#include <mutex>
#include <unordered_map>

#include "maze/generation/maze_generator.h"
#include "proto/maze.pb.h"

class client_storage {
public:
    struct stored_type {
        enum image_status {
            no_image,
            image_saving,
            image_saved
        };
        contracts::MazeGeneratorResponse generated;
        maze::maze_generator::area_type generated_area;
        contracts::MazeSolverResponse solved;
        maze::maze_generator::area_type solved_area;
        image_status generated_image_status = no_image;
        std::string generated_image_path;
        image_status solved_image_status = no_image;
        std::string solved_image_path;
    };
    using storage_type =
        std::unordered_map<std::size_t /*id */, stored_type>;
    using callback_type = std::function<void()>;

    client_storage(boost::asio::thread_pool& thread_pool);
    void insert_and_save_generated(std::size_t id, contracts::MazeGeneratorResponse generated, callback_type cb);
    void insert_and_save_solved(std::size_t id, contracts::MazeSolverResponse solved, callback_type cb);
    bool is_generated_saved(std::size_t id) const;
    bool is_solved_saved(std::size_t id) const;
    bool is_solved_saving(std::size_t id) const;
    std::string generated_image_path(std::size_t id) const;
    std::string solved_image_path(std::size_t id) const;
    constexpr std::mutex& lock() const noexcept { return mutex_; }
    constexpr const storage_type& storage() const noexcept { return storage_; }
private:
    storage_type storage_;
    mutable std::mutex mutex_;
    boost::asio::thread_pool& thread_pool_;
};
