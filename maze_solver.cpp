/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "pizza/basic_service.hpp"
#include "pizza/contracts/AnyMessage.pb.h"

#include "proto/maze.pb.h"
#include "maze/solution/solver_factory.h"
#include "maze/serialization/serialization.h"

struct solver_contract {
    using request_type = contracts::MazeSolverRequest;
    using response_type = contracts::MazeSolverResponse;
};

class maze_solver
: public pizza::basic_service<
    maze_solver,
    // Wrapper AnyMessage type
    pizza::AnyMessage,
    // list of contracts
    solver_contract>
{
public:
    void process_message(solver_contract::request_type && request,
        solver_contract::response_type & response)
    {
        std::cout << "Got request for solution" << std::endl;
        auto start = std::chrono::steady_clock::now();

        try {
            const auto& header = request.header();
            auto area = maze::unsolved_from_bytes(request.maze(),
                header.height(), header.width());

            auto solver = maze::make_solver(request.algorithm());
            solver->solve(area,
                header.entrance_y(), header.entrance_x(),
                header.exit_y(), header.exit_x());

            auto bytes = maze::to_bytes(area, maze::serialization_type::solved);
            
            response.set_solution(std::move(bytes));
            response.set_algorithm(request.algorithm());
        }
        catch (const std::exception& ex) {
            response.set_error(ex.what());
        }
        response.mutable_header()->CopyFrom(request.header());

        auto end = std::chrono::steady_clock::now();
        std::cout << "Finished" << std::endl;
        response.set_elapsed_ms(
            std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
            .count());
    }
};

int main(int argc, char * argv[])
{
    try {
        pizza::command_line_args args(argc, argv);
        if (args.wanted_help())
            return 0;
        maze_solver service;
        service
            .initialize_from_command_line(std::move(args))
            .start();
        return 0;
    }
    catch (const pizza::missing_argument_exception& e) {
        std::cout << "Error: " <<  e.what() << std::endl << e.help() << std::endl;
        return 1;
    }
    catch (const std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}
